package com.backbasetestproject.domain.utils;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:58
 */
public class Observable<T> extends java.util.Observable {

    public void setNewData(T data) {
        setChanged();
        notifyObservers(data);
        clearChanged();
    }

    public void setError(Throwable t) {
        setChanged();
        notifyObservers(t);
        clearChanged();
    }
}