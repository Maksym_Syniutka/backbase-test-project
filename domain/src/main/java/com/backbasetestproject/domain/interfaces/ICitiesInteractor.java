package com.backbasetestproject.domain.interfaces;

import android.support.annotation.NonNull;

import com.backbasetestproject.data.entities.CityInfo;
import com.backbasetestproject.domain.utils.Observable;

import java.io.InputStream;
import java.util.List;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 17:03
 */
public interface ICitiesInteractor {
    @NonNull
    Observable<List<CityInfo>> parseFile(@NonNull InputStream inputStream);

    @NonNull
    Observable<List<CityInfo>> searchCities(String query);
}
