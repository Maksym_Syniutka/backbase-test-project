package com.backbasetestproject.domain.implementations;

import com.backbasetestproject.domain.interfaces.IBaseInteractor;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 17:02
 */
abstract class BaseInteractorImpl implements IBaseInteractor {
    protected final String TAG = this.getClass().getSimpleName();
}
