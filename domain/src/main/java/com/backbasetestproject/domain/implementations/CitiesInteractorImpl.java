package com.backbasetestproject.domain.implementations;

import android.support.annotation.NonNull;
import android.util.Log;

import com.backbasetestproject.data.datastructures.CityInfoTrie;
import com.backbasetestproject.data.entities.CityInfo;
import com.backbasetestproject.domain.interfaces.ICitiesInteractor;
import com.backbasetestproject.domain.utils.Observable;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 17:03
 */
public class CitiesInteractorImpl extends BaseInteractorImpl implements ICitiesInteractor {

    @NonNull
    private CityInfoTrie trie = new CityInfoTrie();

    @Override
    @NonNull
    public Observable<List<CityInfo>> parseFile(@NonNull InputStream inputStream) {
        Observable<List<CityInfo>> observable = new Observable<>();

        Executors.newSingleThreadExecutor().execute(() -> {
            Log.d(TAG, "parseFile -> starting parsing file");
            long fileParseStartTime = System.currentTimeMillis();
            try {
                JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
                Gson gson = new Gson();
                reader.beginArray();
                while (reader.hasNext()) {
                    CityInfo info = gson.fromJson(reader, CityInfo.class);
                    trie.insert(info);
                }
                reader.endArray();
                reader.close();

                Log.d(TAG, String.format(
                        "parseFile -> it took %d seconds to parse the file",
                        (System.currentTimeMillis() - fileParseStartTime) / 1000)
                );

                observable.setNewData(trie.getAllCities());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return observable;
    }

    @NonNull
    @Override
    public Observable<List<CityInfo>> searchCities(String query) {
        Observable<List<CityInfo>> observable = new Observable<>();

        Executors.newSingleThreadExecutor().execute(() -> {
            long searchStart = System.currentTimeMillis();

            List<CityInfo> searchResults = trie.findCitiesWithPrefix(query);

            Log.d(TAG, String.format(
                    "trie search -> it took %d seconds to find \"%s\", result size: %d",
                    (System.currentTimeMillis() - searchStart) / 1000, query, searchResults.size())
            );

            observable.setNewData(searchResults);
        });

        return observable;
    }
}
