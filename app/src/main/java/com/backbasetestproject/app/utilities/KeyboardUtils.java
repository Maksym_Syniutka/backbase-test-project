package com.backbasetestproject.app.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * @author Maksym Syniutka
 * Date: Fri 25-Jan-2019
 * Time: 02:51
 */
public class KeyboardUtils {

    public static void hideKeyboard(Activity activity) {
        hideKeyboard(activity.findViewById(android.R.id.content));
    }

    public static void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
