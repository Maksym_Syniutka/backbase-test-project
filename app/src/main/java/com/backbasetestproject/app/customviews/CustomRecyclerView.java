package com.backbasetestproject.app.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.backbasetestproject.app.R;
import com.backbasetestproject.app.recyclerview.itemdecoration.MarginItemDecorator;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 19:00
 */
public class CustomRecyclerView extends RecyclerView {

    public CustomRecyclerView(@NonNull Context context) {
        super(context);
    }

    public CustomRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public void setItemMargin(int margin) {
        addItemDecoration(new MarginItemDecorator(margin));
    }

    public void setItemMargin(int left, int right, int top, int bottom) {
        addItemDecoration(new MarginItemDecorator(left, right, top, bottom));
    }

    public void enableDividers(int orientation) {
        addItemDecoration(new DividerItemDecoration(getContext(), orientation));
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CustomRecyclerView);

        int marginLeft = array.getDimensionPixelSize(R.styleable.CustomRecyclerView_item_margin_left, 0);
        int marginRight = array.getDimensionPixelSize(R.styleable.CustomRecyclerView_item_margin_right, 0);
        int marginTop = array.getDimensionPixelSize(R.styleable.CustomRecyclerView_item_margin_top, 0);
        int marginBottom = array.getDimensionPixelSize(R.styleable.CustomRecyclerView_item_margin_bottom, 0);
        int margin = array.getDimensionPixelSize(R.styleable.CustomRecyclerView_item_margin, -1);

        if (array.hasValue(R.styleable.CustomRecyclerView_show_dividers)) {
            enableDividers(array.getInt(R.styleable.CustomRecyclerView_show_dividers, VERTICAL));
        }

        if (margin != -1) {
            setItemMargin(margin);
        } else if (marginLeft != 0 || marginRight != 0 || marginTop != 0 || marginBottom != 0) {
            setItemMargin(marginLeft, marginRight, marginTop, marginBottom);
        }

        array.recycle();
    }
}
