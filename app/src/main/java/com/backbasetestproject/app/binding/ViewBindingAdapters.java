package com.backbasetestproject.app.binding;

import android.databinding.BindingAdapter;
import android.view.View;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:46
 */
public class ViewBindingAdapters {
    @BindingAdapter("goneUnless")
    public static void goneUnless(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
