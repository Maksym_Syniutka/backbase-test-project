package com.backbasetestproject.app.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.backbasetestproject.data.entities.CityInfo;
import com.backbasetestproject.data.utils.Resource;
import com.backbasetestproject.domain.implementations.CitiesInteractorImpl;
import com.backbasetestproject.domain.interfaces.ICitiesInteractor;
import com.backbasetestproject.domain.utils.Observable;

import java.io.InputStream;
import java.util.List;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:57
 */
public class SharedViewModel extends BaseViewModel {
    private final ICitiesInteractor citiesInteractor;

    private MutableLiveData<Resource<List<CityInfo>>> parsedCities;

    private MutableLiveData<Resource<List<CityInfo>>> searchResult;

    public SharedViewModel() {
        citiesInteractor = new CitiesInteractorImpl();

        parsedCities = new MutableLiveData<>();
        searchResult = new MutableLiveData<>();
    }

    public void parseCities(@NonNull InputStream inputStream) {
        Log.d(TAG, "starting parsing cities");

        Observable citiesObservable = citiesInteractor.parseFile(inputStream);
        addObservable(citiesObservable);
        citiesObservable.addObserver(createObserver(parsedCities));
    }

    public void search(String search) {
        Log.d(TAG, "starting search for: " + search);

        Observable citiesObservable = citiesInteractor.searchCities(search);
        addObservable(citiesObservable);
        citiesObservable.addObserver(createObserver(searchResult));
    }

    public LiveData<Resource<List<CityInfo>>> getParsedCitiesLiveData() {
        return parsedCities;
    }

    public LiveData<Resource<List<CityInfo>>> getSearchResultLiveData() {
        return searchResult;
    }
}
