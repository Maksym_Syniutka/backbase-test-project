package com.backbasetestproject.app.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.backbasetestproject.data.utils.Resource;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:56
 */
abstract class BaseViewModel extends ViewModel {
    public final String TAG = this.getClass().getSimpleName();

    private final ArrayList<Observable> observables = new ArrayList<>();

    @Override
    protected void onCleared() {
        for (Observable observable : observables) {
            observable.deleteObservers();
        }
        observables.clear();
        super.onCleared();
    }

    protected void addObservable(Observable observable) {
        observables.add(observable);
    }

    protected <T> Observer createObserver(MutableLiveData<Resource<T>> liveData) {
        liveData.postValue(Resource.loading(null));
        return (observable, arg) -> {
            if (arg instanceof Throwable) {
                liveData.postValue(Resource.error((Throwable) arg));
            } else {
                liveData.postValue(Resource.success((T) arg));
            }
        };
    }
}

