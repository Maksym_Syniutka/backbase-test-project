package com.backbasetestproject.app.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backbasetestproject.app.R;
import com.backbasetestproject.app.databinding.FragmentSplashScreenBinding;
import com.backbasetestproject.app.fragments.callbacks.SplashScreenFragmentCallback;
import com.backbasetestproject.app.viewmodel.SharedViewModel;
import com.backbasetestproject.data.utils.State;

import java.io.IOException;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 18:43
 */
public class SplashScreenFragment extends BaseFragment {
    private SplashScreenFragmentCallback fragmentCallback;
    private FragmentSplashScreenBinding binding;
    private SharedViewModel sharedViewModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (!(context instanceof SplashScreenFragmentCallback)) {
            throw new ClassCastException(String.format(
                    "Parent Activity must implement %s",
                    SplashScreenFragmentCallback.class.getSimpleName()
            ));
        } else {
            fragmentCallback = (SplashScreenFragmentCallback) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash_screen, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (sharedViewModel.getParsedCitiesLiveData().getValue() == null && getActivity() != null) {
            Log.d(TAG, "parsed cities data is empty, requesting to parse the file");
            try {
                sharedViewModel.parseCities(getActivity().getAssets().open("cities.json"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    void initViewModel() {
        if (getActivity() == null) {
            Log.e(TAG, "activity == null, cannot initialize view models");
            return;
        }

        sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
    }

    @Override
    void subscribeToViewModel() {
        sharedViewModel.getParsedCitiesLiveData().observe(this, cities -> {
            if (cities == null) {
                Log.e(TAG, "cities list is null");
                return;
            }

            processResponseState(cities.getState());

            if (cities.getState() instanceof State.SUCCESS) {
                Log.d(TAG, String.format("cities list received, cities quantity: %d", cities.getData().size()));
                fragmentCallback.startMainFragment();
            }
        });
    }
}
