package com.backbasetestproject.app.fragments.callbacks;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 18:45
 */
public interface SplashScreenFragmentCallback {
    void startMainFragment();
}
