package com.backbasetestproject.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.backbasetestproject.app.activities.BaseActivity;
import com.backbasetestproject.data.utils.State;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:52
 */
public abstract class BaseFragment extends Fragment {
    public final String TAG = this.getClass().getSimpleName();

    abstract void subscribeToViewModel();

    abstract void initViewModel();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViewModel();
        subscribeToViewModel();
    }

    public void showLoading() {
        getBaseActivity().showLoading();
    }

    public void hideLoading() {
        getBaseActivity().hideLoading();
    }

    public void processResponseState(State state) {
        getBaseActivity().processResponseState(state);
    }

    public BaseActivity getBaseActivity() {
        return ((BaseActivity) getActivity());
    }
}
