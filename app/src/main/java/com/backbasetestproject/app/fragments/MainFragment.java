package com.backbasetestproject.app.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.backbasetestproject.app.R;
import com.backbasetestproject.app.databinding.FragmentMainBinding;
import com.backbasetestproject.app.fragments.callbacks.MainFragmentCallback;
import com.backbasetestproject.app.recyclerview.adapters.CityInfoRecyclerAdapter;
import com.backbasetestproject.app.recyclerview.callbacks.CityInfoClickListener;
import com.backbasetestproject.app.utilities.KeyboardUtils;
import com.backbasetestproject.app.viewmodel.SharedViewModel;
import com.backbasetestproject.data.entities.CityInfo;
import com.backbasetestproject.data.utils.State;
import com.google.android.gms.maps.model.LatLng;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 19:03
 */
public class MainFragment extends BaseFragment implements CityInfoClickListener {

    private MainFragmentCallback fragmentCallback;
    private SharedViewModel sharedViewModel;
    private CityInfoRecyclerAdapter adapter;
    private FragmentMainBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new CityInfoRecyclerAdapter(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (!(context instanceof MainFragmentCallback)) {
            throw new ClassCastException(String.format(
                    "Parent Activity must implement %s",
                    MainFragmentCallback.class.getSimpleName()
            ));
        } else {
            fragmentCallback = (MainFragmentCallback) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.toolbar.inflateMenu(R.menu.toolbar_main_fragment_menu);
        MenuItem searchItem = binding.toolbar.getMenu().findItem(R.id.search);

        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "submitted search query: " + query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                Log.d(TAG, "search query changed: " + query);
                sharedViewModel.search(query);
                return false;
            }
        });

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    void subscribeToViewModel() {
        sharedViewModel.getParsedCitiesLiveData().observe(this, cities -> {
            if (cities == null) {
                Log.e(TAG, "cities list is null");
                return;
            }

            if (cities.getState() instanceof State.SUCCESS) {
                Log.d(TAG, String.format("on parsed city data received, list size: %d", cities.getData().size()));
                adapter.update(cities.getData());
            }
        });
        sharedViewModel.getSearchResultLiveData().observe(this, cities -> {
            if (cities == null) {
                Log.e(TAG, "cities list is null");
                return;
            }

            if (cities.getState() instanceof State.SUCCESS) {
                Log.d(TAG, String.format("on parsed city data received, list size: %d", cities.getData().size()));
                adapter.update(cities.getData());
            }
        });
    }

    @Override
    void initViewModel() {
        if (getActivity() == null) {
            Log.e(TAG, "activity == null, cannot initialize view models");
            return;
        }

        sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
    }

    @Override
    public void onItemClicked(CityInfo cityInfo) {
        Log.d(TAG, "on item clicked: " + cityInfo.getName());
        binding.getRoot().requestFocus();
        KeyboardUtils.hideKeyboard(binding.getRoot());
        fragmentCallback.startMapFragment(new LatLng(
                cityInfo.getCoordinates().getLatitude(), cityInfo.getCoordinates().getLongitude())
        );
    }
}
