package com.backbasetestproject.app.fragments.callbacks;

import com.google.android.gms.maps.model.LatLng;

/**
 * @author Maksym Syniutka
 * Date: Fri 25-Jan-2019
 * Time: 01:36
 */
public interface MainFragmentCallback {
    void startMapFragment(LatLng latLng);
}
