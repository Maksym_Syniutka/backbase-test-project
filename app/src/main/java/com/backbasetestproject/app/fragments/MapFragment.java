package com.backbasetestproject.app.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backbasetestproject.app.R;
import com.backbasetestproject.app.databinding.FragmentMapBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * @author Maksym Syniutka
 * Date: Fri 25-Jan-2019
 * Time: 01:17
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {
    private static final String LOCATION_POINT = "location_point";

    private FragmentMapBinding binding;
    private GoogleMap googleMap;
    private LatLng latLng;

    public static MapFragment getInstance(LatLng latLng) {
        Bundle bundle = new Bundle();
        MapFragment fragment = new MapFragment();

        bundle.putParcelable(LOCATION_POINT, latLng);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) latLng = getArguments().getParcelable(LOCATION_POINT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false);

        binding.map.onCreate(savedInstanceState);
        binding.map.onResume();

        binding.map.getMapAsync(this);

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.map.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.map.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.map.onLowMemory();
    }

    @Override
    public void onDestroy() {
        if (binding.map != null) binding.map.onDestroy();

        if (googleMap != null) {
            googleMap.clear();
            googleMap = null;
        }

        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLngBounds bounds = new LatLngBounds.Builder().include(latLng).build();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 5));
        googleMap.addMarker(new MarkerOptions().position(latLng));

        googleMap.setOnMapLoadedCallback(() -> {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 10));
        });
    }
}
