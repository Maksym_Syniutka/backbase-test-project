package com.backbasetestproject.app.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.backbasetestproject.app.R;
import com.backbasetestproject.app.fragments.MainFragment;
import com.backbasetestproject.app.fragments.MapFragment;
import com.backbasetestproject.app.fragments.SplashScreenFragment;
import com.backbasetestproject.app.fragments.callbacks.MainFragmentCallback;
import com.backbasetestproject.app.fragments.callbacks.SplashScreenFragmentCallback;
import com.backbasetestproject.app.viewmodel.SharedViewModel;
import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends BaseActivity implements SplashScreenFragmentCallback, MainFragmentCallback {

    private SharedViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDataBindingContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(SharedViewModel.class);

        if (savedInstanceState == null) setFragment(new SplashScreenFragment());
    }

    @Override
    public void startMainFragment() {
        replaceFragment(new MainFragment());
    }

    @Override
    public void startMapFragment(LatLng latLng) {
        setFragment(MapFragment.getInstance(latLng), true);
    }
}
