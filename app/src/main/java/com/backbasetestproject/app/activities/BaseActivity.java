package com.backbasetestproject.app.activities;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.backbasetestproject.app.R;
import com.backbasetestproject.app.databinding.ActivityBaseBinding;
import com.backbasetestproject.data.utils.State;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:41
 */
public abstract class BaseActivity extends AppCompatActivity {

    private ActivityBaseBinding baseBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
    }

    public void showLoading() {
        baseBinding.baseActivityLoadProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        baseBinding.baseActivityLoadProgressBar.setVisibility(View.GONE);
    }

    public void processResponseState(State state) {
        baseBinding.setState(state);
    }

    protected void setFragment(Fragment fragment) {
        setFragment(fragment, false);
    }

    protected void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, false);
    }

    protected void setFragment(Fragment fragment, boolean addToBackStack) {
        String tag = fragment.getClass().getSimpleName();

        if (getSupportFragmentManager().findFragmentByTag(tag) != null) return;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .add(baseBinding.baseActivityBaseContentLayout.getId(), fragment, tag);

        if (addToBackStack) transaction.addToBackStack(tag);

        transaction.commit();
    }

    protected void replaceFragment(Fragment fragment, boolean addToBackStack) {
        String tag = fragment.getClass().getSimpleName();

        if (getSupportFragmentManager().findFragmentByTag(tag) != null) return;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                .replace(baseBinding.baseActivityBaseContentLayout.getId(), fragment, tag);

        if (addToBackStack) transaction.addToBackStack(tag);

        transaction.commit();
    }

    protected <T extends ViewDataBinding> T setDataBindingContentView(@LayoutRes int layoutResId) {
        return DataBindingUtil.inflate(getLayoutInflater(), layoutResId, baseBinding.baseActivityBaseContentLayout, true);
    }
}

