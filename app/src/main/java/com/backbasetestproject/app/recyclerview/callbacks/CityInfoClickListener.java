package com.backbasetestproject.app.recyclerview.callbacks;

import com.backbasetestproject.data.entities.CityInfo;

/**
 * @author Maksym Syniutka
 * Date: Fri 25-Jan-2019
 * Time: 00:49
 */
public interface CityInfoClickListener {
    void onItemClicked(CityInfo cityInfo);
}
