package com.backbasetestproject.app.recyclerview.adapters;

import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.backbasetestproject.app.databinding.ItemCityInfoBinding;
import com.backbasetestproject.app.recyclerview.callbacks.CityInfoClickListener;
import com.backbasetestproject.app.recyclerview.comparators.CityInfoSortOrder;
import com.backbasetestproject.app.recyclerview.comparators.CityInfoSortedListImpl;
import com.backbasetestproject.data.entities.CityInfo;

import java.util.List;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 19:27
 * <p>
 * Using SortedList and SortedListAdapterCallback over DiffUtil as it gives more control
 * over add, remove, update data actions within the Adapter.
 */
public class CityInfoRecyclerAdapter extends RecyclerView.Adapter<CityInfoRecyclerAdapter.CityInfoViewHolder> {
    private final String TAG = this.getClass().getSimpleName();

    private final CityInfoClickListener callback;
    private final SortedList<CityInfo> data;

    public CityInfoRecyclerAdapter(CityInfoClickListener callback) {
        CityInfoSortedListImpl cityInfoSortedList = new CityInfoSortedListImpl(
                this, new CityInfoSortOrder.AlphabeticalOrderByCity()
        );
        data = new SortedList<>(CityInfo.class, cityInfoSortedList);
        this.callback = callback;
    }

    @NonNull
    @Override
    public CityInfoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ItemCityInfoBinding binding = ItemCityInfoBinding.inflate(inflater, viewGroup, false);
        return new CityInfoViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CityInfoViewHolder cityInfoViewHolder, int i) {
        cityInfoViewHolder.binding.setCityInfo(data.get(i));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void update(List<CityInfo> updatedData) {
        data.beginBatchedUpdates();
        data.clear();
        data.addAll(updatedData);
        data.endBatchedUpdates();
    }

    class CityInfoViewHolder extends RecyclerView.ViewHolder {
        private ItemCityInfoBinding binding;

        CityInfoViewHolder(@NonNull ItemCityInfoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.setCallback(callback);
        }
    }
}
