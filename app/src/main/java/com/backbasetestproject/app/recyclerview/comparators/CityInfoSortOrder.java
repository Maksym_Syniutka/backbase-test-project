package com.backbasetestproject.app.recyclerview.comparators;

import com.backbasetestproject.data.entities.CityInfo;

import java.util.Comparator;

/**
 * @author Maksym Syniutka
 * Date: Fri 25-Jan-2019
 * Time: 00:06
 * <p>
 * Class for implementing multiple order types for {@link CityInfo}
 */
public class CityInfoSortOrder {

    public static class AlphabeticalOrderByCity implements Comparator<CityInfo> {
        @Override
        public int compare(CityInfo newCityInfo, CityInfo oldCityInfo) {
            return newCityInfo.getName().compareToIgnoreCase(oldCityInfo.getName());
        }
    }
}
