package com.backbasetestproject.app.recyclerview.comparators;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;

import java.util.Comparator;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 23:58
 */
public abstract class SortedListComparatorWrapper<T> extends SortedListAdapterCallback<T> {
    private Comparator<T> comparator;

    public SortedListComparatorWrapper(RecyclerView.Adapter<?> adapter, Comparator<T> comparator) {
        super(adapter);
        this.comparator = comparator;
    }

    @Override
    public int compare(T o1, T o2) {
        return comparator.compare(o1, o2);
    }
}
