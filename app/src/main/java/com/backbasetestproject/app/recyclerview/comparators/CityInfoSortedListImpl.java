package com.backbasetestproject.app.recyclerview.comparators;

import android.support.v7.widget.RecyclerView;

import com.backbasetestproject.data.entities.CityInfo;

import java.util.Comparator;

/**
 * @author Maksym Syniutka
 * Date: Fri 25-Jan-2019
 * Time: 00:01
 */
public class CityInfoSortedListImpl extends SortedListComparatorWrapper<CityInfo> {

    /**
     * @param adapter The {@link RecyclerView.Adapter} instance which should receive events from the SortedList
     * @param order   {@link CityInfoSortOrder} order by which items should be placed within the SortedList
     */
    public CityInfoSortedListImpl(RecyclerView.Adapter<?> adapter, Comparator<CityInfo> order) {
        super(adapter, order);
    }

    @Override
    public boolean areContentsTheSame(CityInfo newCityInfo, CityInfo oldCityInfo) {
        return newCityInfo.equals(oldCityInfo);
    }

    @Override
    public boolean areItemsTheSame(CityInfo newCityInfo, CityInfo oldCityInfo) {
        return newCityInfo.getId() == oldCityInfo.getId();
    }
}
