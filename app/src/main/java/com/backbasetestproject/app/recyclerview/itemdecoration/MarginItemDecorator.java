package com.backbasetestproject.app.recyclerview.itemdecoration;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 19:00
 */
public class MarginItemDecorator extends RecyclerView.ItemDecoration {
    private int left;
    private int right;
    private int top;
    private int bottom;

    public MarginItemDecorator(int margin) {
        this.left = margin;
        this.right = margin;
        this.top = margin;
        this.bottom = margin;
    }

    public MarginItemDecorator(int left, int right, int top, int bottom) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.left = left;
        outRect.right = right;
        outRect.top = top;
        outRect.bottom = bottom;
    }
}