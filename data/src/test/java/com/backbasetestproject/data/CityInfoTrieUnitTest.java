package com.backbasetestproject.data;

import com.backbasetestproject.data.datastructures.CityInfoTrie;
import com.backbasetestproject.data.entities.CityInfo;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 17:09
 */
public class CityInfoTrieUnitTest {

    @Test
    public void check_data_insertion() {
        CityInfoTrie trie = new CityInfoTrie();

        CityInfo one = new CityInfo();
        one.setName("one");
        CityInfo two = new CityInfo();
        two.setName("two");
        CityInfo three = new CityInfo();
        three.setName("three");

        trie.insert(one);
        trie.insert(two);
        trie.insert(three);

        assertEquals(3, trie.getAllCities().size());
    }

    @Test
    public void check_data_insertion_with_collisions() {
        CityInfoTrie trie = new CityInfoTrie();

        CityInfo one = new CityInfo();
        one.setName("one");
        CityInfo two = new CityInfo();
        two.setName("two");
        CityInfo three = new CityInfo();
        three.setName("three");
        CityInfo threeCopy = new CityInfo();
        threeCopy.setName("three");

        trie.insert(one);
        trie.insert(two);
        trie.insert(three);

        assertEquals(3, trie.getAllCities().size());
    }

    @Test
    public void check_data_search() {
        CityInfoTrie trie = new CityInfoTrie();

        CityInfo lviv = new CityInfo();
        lviv.setName("Lviv");
        CityInfo kyiv = new CityInfo();
        kyiv.setName("Kyiv");
        CityInfo london = new CityInfo();
        london.setName("London");
        CityInfo sydney = new CityInfo();
        sydney.setName("Sydney");
        CityInfo swansea = new CityInfo();
        swansea.setName("Swansea");
        CityInfo singapore = new CityInfo();
        singapore.setName("Singapore");

        trie.insert(lviv);
        trie.insert(kyiv);
        trie.insert(london);
        trie.insert(sydney);
        trie.insert(swansea);
        trie.insert(singapore);

        assertEquals(6, trie.getAllCities().size());
        assertEquals(2, trie.findCitiesWithPrefix("L").size());
        assertEquals(3, trie.findCitiesWithPrefix("S").size());
        assertEquals(1, trie.findCitiesWithPrefix("Si").size());
    }

    @Test
    public void check_cities_name_retrieval() {
        CityInfoTrie trie = new CityInfoTrie();

        CityInfo lviv = new CityInfo();
        lviv.setName("Lviv");
        CityInfo kyiv = new CityInfo();
        kyiv.setName("Kyiv");
        CityInfo london = new CityInfo();
        london.setName("London");
        CityInfo sydney = new CityInfo();
        sydney.setName("Sydney");
        CityInfo swansea = new CityInfo();
        swansea.setName("Swansea");
        CityInfo singapore = new CityInfo();
        singapore.setName("Singapore");

        trie.insert(lviv);
        trie.insert(kyiv);
        trie.insert(london);
        trie.insert(sydney);
        trie.insert(swansea);
        trie.insert(singapore);

        List<String> cityNames = trie.getAllCitiesNames();

        assertTrue(cityNames.contains("Lviv"));
        assertTrue(cityNames.contains("Kyiv"));
        assertTrue(cityNames.contains("London"));
        assertTrue(cityNames.contains("Sydney"));
        assertTrue(cityNames.contains("Swansea"));
        assertTrue(cityNames.contains("Singapore"));
    }

    @Test
    public void check_invalid_city_insertion() {
        CityInfoTrie trie = new CityInfoTrie();

        CityInfo lviv = new CityInfo();
        lviv.setName("Lviv");
        CityInfo kyiv = new CityInfo();
        kyiv.setName("Kyiv");
        CityInfo london = new CityInfo();
        london.setName("London");
        CityInfo sydney = new CityInfo();
        sydney.setName("Sydney");
        CityInfo swansea = new CityInfo();
        swansea.setName("Swansea");
        CityInfo singapore = new CityInfo();
        singapore.setName("Singapore");
        CityInfo invalid = new CityInfo();
        invalid.setName("`123");

        trie.insert(lviv);
        trie.insert(kyiv);
        trie.insert(london);
        trie.insert(sydney);
        trie.insert(swansea);
        trie.insert(singapore);
        trie.insert(invalid);

        assertEquals(7, trie.getAllCities().size());
    }

    @Test
    public void check_invalid_city_search() {
        CityInfoTrie trie = new CityInfoTrie();

        CityInfo lviv = new CityInfo();
        lviv.setName("Lviv");
        CityInfo kyiv = new CityInfo();
        kyiv.setName("Kyiv");
        CityInfo london = new CityInfo();
        london.setName("London");
        CityInfo sydney = new CityInfo();
        sydney.setName("Sydney");
        CityInfo swansea = new CityInfo();
        swansea.setName("Swansea");
        CityInfo singapore = new CityInfo();
        singapore.setName("Singapore");
        CityInfo invalid = new CityInfo();
        invalid.setName("`123");
        CityInfo invalid2 = new CityInfo();
        invalid2.setName(null);
        CityInfo invalid3 = new CityInfo();
        invalid3.setName("укр");
        CityInfo invalid4 = new CityInfo();
        invalid4.setName("    -1     всввфві dfaksdjkasdj");

        trie.insert(lviv);
        trie.insert(kyiv);
        trie.insert(london);
        trie.insert(sydney);
        trie.insert(swansea);
        trie.insert(singapore);
        trie.insert(invalid);
        trie.insert(invalid2);
        trie.insert(invalid3);
        trie.insert(invalid4);

        //The size of the tree must be 8, as null cannot be passed as valid value
        assertEquals(9, trie.getAllCities().size());
        assertEquals(1, trie.findCitiesWithPrefix("`").size());
        assertEquals(0, trie.findCitiesWithPrefix("123").size());
        assertEquals(9, trie.findCitiesWithPrefix("").size());
        assertEquals(1, trie.findCitiesWithPrefix("ук").size());
        assertEquals(0, trie.findCitiesWithPrefix("-1").size());
    }
}
