package com.backbasetestproject.data.entities;

import com.google.gson.annotations.SerializedName;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:36
 */
public class Coordinates {
    @SerializedName("lon")
    private Double longitude;
    @SerializedName("lat")
    private Double latitude;

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
