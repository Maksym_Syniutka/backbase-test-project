package com.backbasetestproject.data.datastructures;

import android.support.annotation.NonNull;

import com.backbasetestproject.data.entities.CityInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 17:08
 * <p>
 * <p>
 * Simplest implementation of Trie data structure.
 * Using Trie data structure because it lets you to insert and find strings in O(n), where n being the
 * longest string in the structure.
 */
@SuppressWarnings("WeakerAccess")
public class CityInfoTrie {
    @NonNull
    private final CityInfoTrieNode root;

    public CityInfoTrie() {
        root = new CityInfoTrieNode(' ');
    }

    public void insertAll(@NonNull List<CityInfo> infoList) {
        for (CityInfo cityInfo : infoList) {
            insert(cityInfo);
        }
    }

    public void insert(@NonNull CityInfo cityInfo) {
        String word = cityInfo.getName();
        if (word == null || contains(word)) return;

        CityInfoTrieNode current = root;
        CityInfoTrieNode temp;
        for (char ch : word.toCharArray()) {
            temp = current;
            CityInfoTrieNode child = current.getChild(ch);
            if (child != null) {
                current = child;
                child.setParent(temp);
            } else {
                CityInfoTrieNode node = new CityInfoTrieNode(ch);
                node.setCityInfo(cityInfo);
                current.getChildren().add(node);
                current = current.getChild(ch);
                current.setParent(temp);
            }
        }
        current.isEnd = true;
    }

    public boolean contains(@NonNull String word) {
        CityInfoTrieNode current = root;
        for (char ch : word.toCharArray()) {
            if (current.getChild(ch) == null) return false;
            else current = current.getChild(ch);
        }
        return current.isEnd;
    }

    public List<CityInfo> findCitiesWithPrefix(@NonNull String prefix) {
        CityInfoTrieNode lastNode = root;
        for (int i = 0; i < prefix.length(); i++) {
            lastNode = lastNode.getChild(prefix.charAt(i));
            if (lastNode == null) return new ArrayList<>();
        }

        return lastNode.getCitiesList();
    }

    public List<String> getAllCitiesNames() {
        return root.getCitiesNames();
    }

    public List<CityInfo> getAllCities() {
        return root.getCitiesList();
    }
}
