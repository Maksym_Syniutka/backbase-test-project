package com.backbasetestproject.data.datastructures;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.backbasetestproject.data.entities.CityInfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 17:05
 */
class CityInfoTrieNode {
    @NonNull
    private LinkedList<CityInfoTrieNode> children;
    private CityInfoTrieNode parent;
    private CityInfo cityInfo;
    private char data;
    boolean isEnd;

    CityInfoTrieNode(char c) {
        children = new LinkedList<>();
        data = c;
    }

    @Nullable
    CityInfoTrieNode getChild(char c) {
        for (CityInfoTrieNode eachChild : children) {
            if (eachChild.data == c) return eachChild;
        }
        return null;
    }

    @NonNull
    List<String> getCitiesNames() {
        List<String> list = new ArrayList<>();

        if (isEnd) list.add(convertToString());

        for (CityInfoTrieNode node : children) {
            list.addAll(node.getCitiesNames());
        }
        return list;
    }

    @NonNull
    List<CityInfo> getCitiesList() {
        List<CityInfo> citiesList = new ArrayList<>();

        if (isEnd) citiesList.add(cityInfo);

        for (CityInfoTrieNode node : children) {
            citiesList.addAll(node.getCitiesList());
        }
        return citiesList;
    }

    @NonNull
    LinkedList<CityInfoTrieNode> getChildren() {
        return children;
    }

    void setParent(CityInfoTrieNode parent) {
        this.parent = parent;
    }

    void setCityInfo(CityInfo cityInfo) {
        this.cityInfo = cityInfo;
    }

    private String convertToString() {
        return parent == null ? "" : parent.convertToString() + new String(new char[]{data});
    }
}

