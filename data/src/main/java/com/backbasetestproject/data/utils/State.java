package com.backbasetestproject.data.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:32
 *
 * Class representing current state of data.
 *
 * {@link State.LOADING} - uses for the data which currently is being loading
 * {@link State.SUCCESS} - uses for the data which has been successfully retrived
 * {@link State.ERROR}   - uses to represent an error which occured during data retrieval
 */
public class State {
    @Nullable
    private Throwable throwable;

    private State(@Nullable Throwable throwable) {
        this.throwable = throwable;
    }

    @Nullable
    public Throwable getThrowable() {
        return throwable;
    }

    public static class LOADING extends State {
        public LOADING() {
            super(null);
        }
    }

    public static class SUCCESS extends State {
        SUCCESS() {
            super(null);
        }
    }

    public static class ERROR extends State {
        public ERROR(@NonNull Throwable throwable) {
            super(throwable);
        }
    }
}
