package com.backbasetestproject.data.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Maksym Syniutka
 * Date: Thu 24-Jan-2019
 * Time: 16:34
 *
 * A container which contains info about the current state of the data and if present, the data itself.
 */
public class Resource<T> {
    public static <T> Resource<T> success(@Nullable T data) {
        return new Resource<>(data, new State.SUCCESS());
    }

    public static <T> Resource<T> error(@NonNull Throwable throwable) {
        return new Resource<>(null, new State.ERROR(throwable));
    }

    public static <T> Resource<T> loading(@Nullable T data) {
        return new Resource<>(data, new State.LOADING());
    }

    @Nullable
    private T data;
    @NonNull
    private State state;

    private Resource(@Nullable T data, @NonNull State state) {
        this.data = data;
        this.state = state;
    }

    @Nullable
    public T getData() {
        return data;
    }

    @NonNull
    public State getState() {
        return state;
    }
}

